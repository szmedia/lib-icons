const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// Bugfix: https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/518
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const TerserPlugin = require('terser-webpack-plugin')

// Configuration: Paths
const config = {
    ext: {
        name: 'av_alvium_landingpage',
        path: './'
    },
    vendor: {
        node_modules : 'node_modules/'
    },
};

const paths = {
    private: {
        scss: config.ext.path + '/Resources/Private/Scss/',
        js: config.ext.path + '/Resources/Private/JavaScript/'
    },
    public: {
        css: 'Css/',
        js: 'JavaScript/',
        fonts: 'Fonts/',
        images: 'Images/',
    },
    privateExtensions : {
        js: config.ext.path + '/Resources/Private/Extensions/'
    },
    output: config.ext.path + '../Resources/Public/Build/',
    ext : './web/typo3conf/ext/'
};

// Webpack Configuration
module.exports = (env, argv) => (
    {
        watch: false,
        watchOptions: {
            ignored: '/node_modules/',
            poll: 1000
        },
        entry: {
            app: paths.private.js + 'index.js' ,
            main: paths.private.scss + 'index.scss'
        },
        output: {
            path: path.resolve(__dirname, paths.output),
            filename: paths.public.js + '[name].js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        {
                            loader : 'style-loader',
                            options: {
                            }
                        },
                        {
                            loader: MiniCssExtractPlugin.loader
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders : 2,
                                sourceMap: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                plugins: [
                                    require('precss'),
                                    require('autoprefixer')()
                                ]
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: false
                            }
                        }
                    ]
                },
                {
                    test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: paths.public.fonts,
                                publicPath: '../' + paths.public.fonts
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|gif|jpg)$/,
                    loader: 'file-loader',
                    options: {
                        outputPath: paths.public.images,
                        name: '[name].[ext]'
                    }
                }
            ]
        },
        resolve: {
            symlinks: false,
            modules: ['node_modules']
        },
        optimization: {
            minimize: true,
            minimizer: [
                new TerserPlugin(
                    {
                        parallel: true,
                        terserOptions: {
                            ecma: 6,
                        },
                    }
                ),
            ]
        },
        plugins: [
            new webpack.ProgressPlugin(),
            new FixStyleOnlyEntriesPlugin(),
            new MiniCssExtractPlugin(
                {
                    filename: paths.public.css + '[name].css',
                    chunkFilename: paths.public.css + '[id].[hash].css'
                }
            )
        ],
        devtool : false,
        performance: {
            hints: false
        }
    }
);