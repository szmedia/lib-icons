/**
 * Offcanvas Menu
 * @TODO: Click outside closeMenu()
 */
export class Offcanvas {

  /**
   * Constructor
   * @param selector
   */
  constructor(selector) {
    this.vars = {
      isActive: false,
      menu: document.querySelectorAll('[' + selector + ']'),
      buttons: document.querySelectorAll('[' + selector + '-trigger]'),
      classes: {
        visible: 'is-visible',
        active: 'is-active',
      }
    };

    this.bindEvents();
  };

  /**
   * Bind events for offcanvas menu
   */
  bindEvents = () => {
    let _this = this;

    // Open / Close menu
    this.vars.buttons.forEach(function (button) {
      button.addEventListener('click', () => {
        if(_this.isActive()) {
          _this.hideMenu();
        } else {
          _this.showMenu();
        }
      });
    });
  };

  /**
   * Check, if menu is open
   * @returns {boolean}
   */
  isActive = () => {
    let _this = this;
    return _this.vars.menu[0].classList.contains(_this.vars.classes.visible)
  };

  /**
   * Hide Menu
   */
  hideMenu = () => {
    let _this = this;
    _this.vars.menu[0].classList.remove(_this.vars.classes.visible);
    _this.vars.buttons.forEach(function (button) {
      button.classList.remove(_this.vars.classes.active);
    });
  };

  /**
   * Show Menu
   */
  showMenu = () => {
    let _this = this;
    _this.vars.menu[0].classList.add(_this.vars.classes.visible);
    _this.vars.buttons.forEach(function (button) {
      button.classList.add(_this.vars.classes.active);
    });
  };

}

