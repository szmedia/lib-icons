import 'bootstrap';
import {Offcanvas} from "./libs/offcanvas";

document.addEventListener("DOMContentLoaded",function() {
  new Offcanvas('data-offcanvas');
});
